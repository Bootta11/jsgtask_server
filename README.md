# jsgtask_server

## 1. Project setup
```
npm install
```

### 2. Run migrations
```
knex migrate:latest
```

### 3. Optional: seed test data
```
knex seed:run
```

### 4. Run server
```
npm run serve
```
