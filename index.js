require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const winston = require('winston');
const log = require('./libs/winston.js');
const laravel_helpers = require('laravel-js-helpers');
const compression = require('compression');

const app = express();

const shouldCompress = (req, res) => {
    if (req.headers['x-no-compression']) {
        return false;
    }

    return compression.filter(req, res);
};

app.use(compression({
    filter: shouldCompress,
}));

app.use(function (req, res, next) {
    if(!req.log) req.log = log;
    if(!req.lh) req.lh = laravel_helpers;
    next();
});

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

app.use(require('./routes.js'));

app.use(function logErrors (err, req, res, next) {
    req.log.error(err);
    //console.error('err', err.message !== undefined ? err.message : err, err);
    next(err)
});

app.use(function clientErrorHandler (err, req, res, next) {
    if (req.xhr) {
        res.status(500).send({ error: process.env.ENVIRONMENT === 'production'? 'Something failed!' : err });
    } else {
        next(process.env.ENVIRONMENT === 'production'? 'Something failed!' : err)
    }
});

app.use(function errorHandler (err, req, res, next) {
    res.status(500);
    let error_msg = (process.env.ENVIRONMENT === 'production'? 'Something failed!' : (err.message !== undefined ? err.message : err));
    res.json({ error: error_msg });
});

app.listen(process.env.PORT, err => {
    if (err) {
        return;
    }
    console.log(`Your server is ready on port ${process.env.PORT}!`);
});
