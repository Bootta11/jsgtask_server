
exports.up = function(knex) {
    return knex.schema
        .createTable('customers', function (table) {
            table.increments('id');
            table.integer('user_id').unsigned().references('id').inTable('users')
            table.string('name', 255).notNullable();
            table.string('lastname', 255).notNullable();
            table.string('email', 255).notNullable();
            table.string('address', 255).notNullable();
        })
};

exports.down = function(knex) {
  return knex.schema.dropTable('customers');
};
