
exports.up = function(knex) {
    return knex.schema
        .createTable('selling_articles', function (table) {
            table.increments('id');
            table.string('title', 255).notNullable();
            table.string('summary', 255).notNullable();
            table.float('price', 255).notNullable();
            table.string('photo', 255).notNullable();
            table.string('category', 255).notNullable();
        })
};

exports.down = function(knex) {
  return knex.schema.dropTable('selling_articles');
};
