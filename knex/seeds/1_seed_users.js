
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {id: 1, email: 'test1@mail.com', password: '$2a$12$tm88fkTxe9IHots9WqVuN.E96nZAwQXZtisikpKbIDEgFcTUI.loe'},
        {id: 2, email: 'test2@mail.com', password: '$2a$12$4TFRz2qKP8Pi8tpQwgfC0.if3afR6Neih.SBbHjB5d5Tb2oS8Plhy'},
        {id: 3, email: 'test3@mail.com', password: '$2a$12$S7I9UFKyUpWVmyKWEbPwweZvQ.nlVjKSA91xsl3eL2LrQmATpIqc2'},
      ]);
    });
};
