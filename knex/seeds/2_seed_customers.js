
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('customers').del()
    .then(function () {
      // Inserts seed entries
      return knex('customers').insert([
        {user_id: 1, name: 'Customer', lastname: 'One', email: 'customer1@mail.com', address: 'Customer 1 address'},
        {user_id: 3, name: 'Customer', lastname: 'Two', email: 'customer2@mail.com', address: 'Customer 2 address'},
      ]);
    });
};
