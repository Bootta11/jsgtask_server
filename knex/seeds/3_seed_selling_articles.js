exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('selling_articles').del()
        .then(function () {
            // Inserts seed entries

            const categories = ['Art', 'Sport', 'Politics', 'Tech'];

            const images_pool = [
                "-I08fnnIM2A.jpg","3vLeKK5Q100.jpg","4AYUp0dLkGA.jpg","8Np57XmObKs.jpg",
                "BlRhuRGULNQ.jpg","CbYCdiz6i_E.jpg","CoL0cGeFZBc.jpg","G7TYJx6OuyA.jpg","HfhNRdqa9FA.jpg",
                "Iz6bore7P0I.jpg","MSrxsEtE5R0.jpg","P-j3j_jtKhw.jpg","SIpx7mJV6tI.jpg","SdEjdeKBHpA.jpg",
                "UTkkRpj5jUE.jpg","YNZ6VEtTUI0.jpg","ZQAABD7WUU0.jpg","cayZEcsNiYI.jpg","cdnHlQt6DLg.jpg",
                "dIYZIWOXi1A.jpg","dMavG4HgL_o.jpg","iD6o6fydAWE.jpg","lI7dlA5VBp8.jpg","m4UunW19iOQ.jpg",
                "mpukLDh-kbo.jpg","shutterstock_100407082.jpg","shutterstock_104366516.jpg",
                "shutterstock_105551657.jpg","shutterstock_108647036.jpg","shutterstock_109410320.jpg",
                "shutterstock_109982438.jpg","shutterstock_1100267.jpg","shutterstock_111126614.jpg",
                "shutterstock_111276581.jpg","shutterstock_133080482.jpg","shutterstock_134967752.jpg",
                "shutterstock_137167187.jpg","shutterstock_143188261.jpg","shutterstock_145795217.jpg",
                "shutterstock_148209497.jpg","shutterstock_149418329.jpg","shutterstock_162567314.jpg",
                "shutterstock_162582668.jpg","shutterstock_198004562.jpg","shutterstock_213444994.jpg",
                "shutterstock_254142166.jpg","shutterstock_2623630.jpg","shutterstock_2666165.jpg",
                "shutterstock_267830435.jpg","shutterstock_272756663.jpg","shutterstock_276380672.jpg",
                "shutterstock_278855822.jpg","shutterstock_284794943.jpg","shutterstock_289654874.jpg",
                "shutterstock_304243817.jpg","shutterstock_304789790.jpg","shutterstock_307219181.jpg",
                "shutterstock_325251248.jpg","shutterstock_334603625.jpg","shutterstock_335473556.jpg",
                "shutterstock_347961782.jpg","shutterstock_358464983.jpg","shutterstock_367586594.jpg",
                "shutterstock_370955504.jpg","shutterstock_373686493.jpg","shutterstock_376879774.jpg",
                "shutterstock_384984838.jpg","shutterstock_491371186.jpg","shutterstock_491387521.jpg",
                "shutterstock_60871054.jpg","shutterstock_69116623.jpg","tSe4FaiYc8s.jpg","uYSvlocDSsg.jpg",
                "vvUy1hWVYEA.jpg","wmY7gKdYSmI.jpg","zFjnuaEcfFw.jpg"
            ];

            let inserts = [];
            categories.forEach(cat => {
                const items_in_cat = Math.round(Math.random() * 10) + 5;
               for(let i = 0; i <= items_in_cat; i++){
                   const item_id = Math.floor(Math.random() * 1000);
                   inserts.push({
                       title: 'Sale Item ' + item_id,
                       summary: 'This is summary of sale item ' + item_id,
                       price: 2 + (Math.random() * 20),
                       photo: images_pool[Math.floor(Math.random() * images_pool.length)],
                       category: categories[Math.floor(Math.random() * categories.length)]
                   })
               }
            });

            return knex('selling_articles').insert(inserts);
        });
};
