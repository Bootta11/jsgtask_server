require('dotenv').config();

module.exports = {
    development: {
        client: process.env.DB_CLIENT || 'mysql',
        connection:
            process.env.DATABASE_URL
            || {
                host: process.env.DB_HOST,
                user: process.env.DB_USER,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_NAME,
            },
        migrations: {
            directory: __dirname + '/knex/migrations',
        },
        seeds: {
            directory: __dirname + '/knex/seeds'
        }
    },
    production: {
        client: process.env.DB_CLIENT || 'mysql',
        connection:
            process.env.DATABASE_URL
            || {
                host: process.env.DB_HOST,
                user: process.env.DB_USER,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_NAME,
            },
        migrations: {
            directory: __dirname + '/knex/migrations',
        },
        seeds: {
            directory: __dirname + '/knex/seeds'
        }
    }
};

