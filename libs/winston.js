const winston = require('winston');
require('winston-daily-rotate-file');

const levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    debug: 4,
};

const level = () => {
    const env = process.env.NODE_ENV || 'development'
    const isDevelopment = env === 'development'
    return isDevelopment ? 'debug' : 'warn'
};

const format = winston.format.combine(
    winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss:ms'}),
    winston.format.errors({ stack: true }),
    winston.format.printf(
        (info) => {
            let label = info.label ? ' [' + info.label + '] ': ' ';
            let complete_message = {message: info.message};
            const splatArgs = info[Symbol.for('splat')] || undefined;

            if(splatArgs) complete_message.splat = splatArgs;
            if(info.data) complete_message.data = info.data;

            if(info.stack ) {
                let err_info = winston.exceptions.getAllInfo(info);
                complete_message.stack = err_info.trace ? err_info.trace : err_info.stack;
            }

            return `${info.timestamp}${label}${info.level} ${JSON.stringify(complete_message)}`
        },
    ),
);

const logs_folder = __dirname + '/../storage/logs/';
const transports = [
    new winston.transports.Console(),
    // new winston.transports.File({
    //     filename: logs_folder + 'jsgserver-error.log',
    //     level: 'error',
    // }),
    // new winston.transports.File({filename: logs_folder + 'jsgserver.log'}),
    new winston.transports.DailyRotateFile({
        filename: logs_folder + 'jsgserver-error-%DATE%.log',
        datePattern: 'YYYY-MM-DD',
        maxSize: '100m',
        maxFiles: '5d',
        level: 'error'
    }),
    new winston.transports.DailyRotateFile({
        filename: logs_folder + 'jsgserver-%DATE%.log',
        datePattern: 'YYYY-MM-DD',
        maxSize: '100m',
        maxFiles: '5d'
    })
];

const Logger = winston.createLogger({
    level: level(),
    levels,
    format,
    transports,
});

module.exports = Logger;
