const jwt = require('jsonwebtoken');
const knex = require('../knex/index.js');

module.exports = {
    authenticateJWT: (req, res, next) => {
        if (req.lh.data_get(req, 'app.locals.fake_auth') === true) {
            knex('users')
                .first()
                .then(user => {
                    req.user = {email: 'fake_user@mail.com'}
                    if (user !== undefined) {
                        req.user = Object.assign(user, {email: 'fake_user@mail.com'});
                    }

                    next();
                })
                .catch(e => {
                    req.user = {email: 'fake_user@mail.com'};
                    next();
                });

            next()
        }
        const authHeader = req.headers.authorization;

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
                if (err) {
                    return res.sendStatus(403);
                }

                req.user = user;
                next();
            });
        } else {
            res.sendStatus(401);
        }
    }
};
