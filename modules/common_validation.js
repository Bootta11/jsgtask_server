const {query} = require('express-validator');

module.exports = {
  pagination: [
      query('page').toInt().default(1),
      query('per_page').toInt().default(50)
  ],
};