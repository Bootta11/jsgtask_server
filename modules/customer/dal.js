const knex = require('../../knex/index.js');

const table_name = 'customers';

module.exports = {
    get: function (id) {
        return knex(table_name)
            .where({id: id})
            .first();
    },
    getAll: function (params) {
        return knex(table_name)
            .paginate({perPage: params.per_page, currentPage: params.page});
    },
    create: function (params) {
        let insert = {
            name: params.name,
            lastname: params.lastname,
            email: params.email,
            address: params.address,
        };

        return knex(table_name)
            .insert(insert)
            .catch(err => {
                return err;
            });

    },
    isUserCustomer(user_id) {
        let query = knex('customers').where('customers.user_id', '=',  user_id);

            return query.first()
                .then(result => result !== undefined)
                .catch(e => false);
    }
};
