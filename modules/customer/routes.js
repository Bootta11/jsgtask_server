const common_validation = require('../common_validation.js');
const {body, validationResult} = require('express-validator');
const cf = require('../common_functions.js');

const module_api_prefix = '/customers';

module.exports = (service) => {
    return (app) => {
        app.use(module_api_prefix, cf.authenticateJWT);

        app.get(module_api_prefix + '/:id', (req, res, next) => {
            try {
                service.get(req).then((response) => res.json(response))
            } catch (e) {
                next(e)
            }
        });

        app.get(module_api_prefix, common_validation.pagination, (req, res, next) => {
            try {
                service.getAll(req).then((response) => res.json(response))
            } catch (e) {
                next(e)
            }
        });

        app.post(module_api_prefix,[
            body('name').not().isEmpty().withMessage('Param is required'),
            body('lastname').not().isEmpty().withMessage('Param is required'),
            body('email').isEmail().withMessage('Not valid email'),
            body('address').not().isEmpty().withMessage('Param is required'),
        ], (req, res, next) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            try {
                service.create(req).then(result => res.json(result))
            } catch (e) {
                next(e)
            }
        })

        app.post(module_api_prefix + '/checkout',[
            body('cart_items').not().isEmpty().withMessage('Cart items are required'),
        ], (req, res, next) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            try {
                service.checkout(req).then(result => res.json(result))
            } catch (e) {
                next(e)
            }
        })
    }
};
