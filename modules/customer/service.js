const dal = require('./dal');

module.exports = {
    get: function (req) {
        return dal.getUser(req.param('id'))
    },
    getAll: function (req) {
        let params = {
            per_page: req.query.per_page,
            page: req.query.page
        };

        return dal.getAll(params)
    },
    create: function (req) {
        return dal.create({
            name: req.body.name,
            lastname: req.body.name,
            email: req.body.email,
            address: req.body.address,
        });

    },
    checkout(req) {
        if (req.user && req.user.id) {
            return dal.isUserCustomer(req.user.id);
        } else {
            return Promise.reject('Missing user id');
        }
    }
};
