const knex = require('../../knex/index.js');

const table_name = 'selling_articles';

module.exports = {
    get: function (id) {
        return knex(table_name)
            .where({id: id})
            .first();
    },
    getAll: function (params) {
        let query = knex(table_name);

        if(params.q !== undefined){
            query.where((q)=>{
                q.where('title', 'like', '%' + params.q + '%');
                q.orWhere('summary', 'like', '%' + params.q + '%')
            });
        }

        if(params.category !== undefined){
            query.where('category', params.category);
        }

        return query.paginate({ perPage: params.per_page, currentPage: params.page });
    }
};
