const {body, query} = require('express-validator');
const common_validation = require('../common_validation.js');
const cf = require('../common_functions.js');

const module_api_prefix = '/selling_articles';

module.exports = (service) => {
    return (app) => {
        app.get(module_api_prefix + '/:id', (req, res, next) => {
            try {
                service.get(req).then((response) => res.json(response))
            } catch (e) {
                next(e)
            }
        });

        app.get(module_api_prefix + '/:id/photo', (req, res, next) => {
            try {
                service.getPhotoPath(req)
                    .then((photo_path) => {
                        let options = {
                            headers: {
                                'x-timestamp': Date.now(),
                                'x-sent': true
                            }
                        };

                        res.sendFile(photo_path, options, function (err) {
                            if (err) {
                                next(err)
                            }
                        })
                    }).catch(e => next(e))
            } catch (e) {
                next(e)
            }
        });

        app.get(module_api_prefix, common_validation.pagination, (req, res, next) => {
            try {
                service.getAll(req).then((response) => res.json(response))
            } catch (e) {
                next(e)
            }
        })


    }
};
