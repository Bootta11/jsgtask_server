const dal = require('./dal');
const path = require('path');

module.exports = {
    get: function(req){
        return dal.get(req.param('id'))
    },
    getAll: function(req){
        let params = {
            per_page: req.query.per_page,
            page: req.query.page,
        };

        if(req.query.q){
            params.q = req.query.q;
        }

        if(req.query.category){
            params.category = req.query.category;
        }

        return dal.getAll(params)
    },
    getPhotoPath: function (req) {
        let {id} = req.params;

        return dal.get(id).then(item => {
            if(item !== undefined){
                return path.join(__dirname, '../../storage/selling_article_photos') + '/' + item.photo;
            }

            return Promise.reject('Not existing id');
        }).catch(e => {return Promise.reject(e)});
    }
};
