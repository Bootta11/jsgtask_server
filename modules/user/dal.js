const knex = require('../../knex/index.js');
const bcrypt = require('bcrypt');
const saltRounds = 12;

const table_name = 'users';

module.exports = {
    get: function (id) {
        return knex(table_name)
            .where({id: id})
            .first();
    },
    getByEmail: function (email) {
        return knex(table_name)
            .where({email: email})
            .first();
    },
    getAll: function (params) {
        return knex(table_name)
            .paginate({perPage: params.per_page, currentPage: params.page});
    },
    create: function (params) {
        let insert = {
            email: params.email,
            password: params.password
        };

        return knex(table_name)
            .insert(insert)
            .catch(err => {
                return err;
            });

    }
};
