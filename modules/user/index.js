const service = require('./service.js');
const routes = require('./routes.js');

module.exports = {
    routes: routes(service)
}