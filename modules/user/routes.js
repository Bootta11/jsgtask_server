const {body, validationResult} = require('express-validator');
const common_validation = require('../common_validation.js');
const cf = require('../common_functions.js');

const module_api_prefix = '/users';

module.exports = (service) => {
    return (app) => {
        app.post('/login', [
            body('email').default(''),
            body('password').default('')
        ], (req, res, next) => {
            try {
                service.login(req)
                    .then((response) => {
                        if(response.errors) res.status(403);
                        return res.json(response);
                    })
                    .catch(e => {
                        next(e)
                    })
            } catch (e) {
                next(e)
            }
        });

        if(process.env.ENVIRONMENT !== 'production'){
            app.post('/login/fake', (req, res, next) => {
                try {
                    service.loginFake(req)
                        .then((response) => res.json(response))
                        .catch(e => {
                            next(e)
                        })
                } catch (e) {
                    next(e)
                }
            });
        }

        app.use(module_api_prefix, cf.authenticateJWT);

        app.get(module_api_prefix + '/:id', (req, res, next) => {
            try {
                service.get(req).then((response) => res.json(response))
            } catch (e) {
                next(e)
            }
        });

        app.get(module_api_prefix,
            common_validation.pagination,
            (req, res, next) => {
                try {
                    service.getAll(req).then((response) => res.json(response))
                } catch (e) {
                    next(e)
                }
            });

        app.post(module_api_prefix, [
            body('email').isEmail(),
            body('password').isLength({min: 6, max: 50}),
        ], (req, res, next) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }

            try {
                service.create(req).then(result => res.json(result))
            } catch (e) {
                next(e)
            }
        })
    }
};
