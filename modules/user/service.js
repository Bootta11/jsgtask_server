const dal = require('./dal');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const jwt = require('jsonwebtoken');

module.exports = {
    login: function (req) {
        const {email, password} = req.body;

        return dal.getByEmail(email)
            .then((user) => {
                if (user) {
                    return bcrypt.compare(password, user.password)
                        .then(function (result) {
                            if (result === true) {
                                return {id: user.id, email: user.email, token: jwt.sign({id: user.id, email: email}, process.env.JWT_SECRET)};
                            } else {
                                return {
                                    errors:['Wrong password']
                                };
                            }
                        });

                } else {
                    return {
                        errors:['Wrong password']
                    };
                }
            }).catch(e => {
                return Promise.reject(e);
            });
    },
    loginFake: function (req) {
        req.app.locals.fake_auth = !req.app.locals.fake_auth;

        if(req.lh.data_get(req, 'body.fake_auth') !== undefined){
            req.app.locals.fake_auth = req.lh.data_get(req, 'body.fake_auth');
        }

        return Promise.resolve({fake_auth: req.app.locals.fake_auth ? 'active' : 'inactive'})
    },
    get: function (req) {
        return dal.get(req.param('id'))
    },
    getAll: function (req) {
        let params = {
            per_page: req.query.per_page,
            page: req.query.page
        };

        return dal.getAll(params)
    },
    create: function (req) {
        return bcrypt.hash(req.body.password, saltRounds)
            .then((hash) => {
                return dal.create({email: req.body.email, password: hash});
            }).catch(e => {
                return e
            });
    }
};
