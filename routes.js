const express = require('express');
const router = express.Router();

const user = require('./modules/user/index.js');
const customer = require('./modules/customer/index.js');
const selling_article = require('./modules/selling_article/index.js');

user.routes(router);
customer.routes(router);
selling_article.routes(router);

module.exports = router;